
abstract class Wave{
  float outbounds;
  float radius;
  PVector pos;
  
  abstract void draw();
  abstract void update();
  
  boolean inBounds(){
    outbounds = PVector.sub(new PVector(this.pos.x, this.pos.y), new PVector(0, 0)).mag()*2 + 25;
    return radius < outbounds;
  }
  
}