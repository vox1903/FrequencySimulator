import ddf.minim.*;
import ddf.minim.ugens.*;

Minim minim;
AudioOutput out;
WaveGenerator waveGenerator;


void setup() {
  size(800, 500);
  background(0);
  waveGenerator = new WaveGenerator();
}

void draw() {
  //background(0);
  noStroke();
  fill(0,30);
  rect(0,0,width,height);
  waveGenerator.update();
}


void keyPressed() {
  if (key == 'z'){waveGenerator.createWave(WavesEnum.SOUNDWAVE,width / 2, height / 2,key);}
  else {waveGenerator.createWave(WavesEnum.SIMPLEWAVE,width / 2, height / 2,key);}
}