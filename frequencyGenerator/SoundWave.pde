
class SoundWave extends Wave{

  int strokeSize;
  float time;
  float acceleration;
  color c;
  int mixRed;
  int mixBlue;
  int mixGreen;
    
  SoundWave(int x, int y){
    this.pos = new PVector(x,y);
    this.acceleration = 1;
    this.radius = 5;
    this.mixRed = int(random(100,255));
    this.mixBlue = int(random(100,255));
    this.mixGreen = int(random(100,255));
    this.c = color(
      map(radius,0,width,0,mixRed),
      map(radius,0,width,0,mixBlue),
      map(radius,0,width,0,mixGreen));
    this.strokeSize = 5;
  }
  
  void draw(){
    pushMatrix();
    pushStyle();
    //translate(pos.x,pos.y);
    noFill();
    strokeWeight(this.strokeSize);
    stroke(this.c);
    
    
    ellipse(this.pos.x,this.pos.y,this.radius,this.radius);
    
    
    popStyle();
    popMatrix();
  }
  
  void update(){
    radius += acceleration;
    this.c = color(
      map(radius,0,width,0,mixRed),
      map(radius,0,width,0,mixBlue),
      map(radius,0,width,0,mixGreen));
  }
  


}