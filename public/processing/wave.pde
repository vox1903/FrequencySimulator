class Wave{
  float sizeR;
  int gaps;
  PVector pos;
  //float frequencies[] = {261.63,293.66,329.63,349.23,392,440,493.88};
  float frequencies[] = {261.63,277.18,293.66,311.13,329.63,349.23,369.99,392,415.3,440,466.16,493.88,523.25};
  float ratio = 1.999961778;

  var osc = [], fft;

  Wave(float x, float y, int gaps){
    pos = new PVector(x,y);
    sizeR = 5;
    this.gaps = gaps;
    //fill(255);
    //textSize(32);
    //text(nomNota(gaps), x-16,y+16);

    for(int i = 0; i < 3; i++) {
        osc[i] = new p5.Oscillator(frequencies[gaps-1], 'sine'); // set frequency and type
        osc[i].amp(.2);
    }
    fft = new p5.FFT();

  }

  void play(){
      sizeR = 5;
      update();
      for(int i = 0; i < 3; i++) {
          osc[i].start();
      }
  }

  string nomNota(int val){
    if(gaps == 1) return "Do";
    if(gaps == 2) return "Re";
    if(gaps == 3) return "Mi";
    if(gaps == 4) return "Fa";
    if(gaps == 5) return "Sol";
    if(gaps == 6) return "La";
    return "Si";
  }

  void update(){
    sizeR += 10;
    for(int i = -1, j = 0; i < 2; i++, j++) {
        osc[j].amp(map(sizeR,5,outbounds,.2,0));
        osc[j].freq(frequencies[gaps-1] * pow(ratio, octave + i ));
    }
  }

  boolean inbounds(){
    return sizeR < outbounds;
  }

  void draw(){
    pushMatrix();
    pushStyle();
    translate(pos.x,pos.y);
    noFill();
    strokeWeight(5);
    stroke(getCol(),map(sizeR,5,outbounds,80,5));
    rotate(map(sizeR,5,outbounds,0,PI));
    gapLipse(0,0,sizeR,gaps);
    popStyle();
    popMatrix();
  }

  void gapLipse(float x, float y, float sizeR, float gaps){
    float gapSize = 360/(2*gaps);
    float sizeArc = (360 - (gaps * gapSize)) / gaps;
    float start, end;
    for(int i = 0; i < gaps; i++){
      start = i * (sizeArc + gapSize);
      end = start + sizeArc;
      arc(x,y,sizeR,sizeR,radians(start),radians(end));
      /* Lineas del centro a los bordes de un segmento
      cent = new PVector(x,y);
      PVector enPoint = PVector.sub(cent,new PVector(sin(radians(end))*sizeR,cos(radians(end))*sizeR));
      PVector stPoint = PVector.sub(cent,new PVector(sin(radians(start))*sizeR,cos(radians(start))*sizeR));
      enPoint.mult(0.5);
      stPoint.mult(0.5);
      line(x,y,stPoint.x,stPoint.y);
      line(x,y,enPoint.x,enPoint.y);*/
    }

  }

  color getCol(){
    /*if(gaps == 1) return color(255,0,0);
    if(gaps == 2) return color(0,255,0);
    if(gaps == 3) return color(0,0,255);
    if(gaps == 4) return color(255,255,0);
    if(gaps == 5) return color(0,255,255);
    if(gaps == 6) return color(255,0,255);
    if(gaps == 7) return color(255,255,255);
    return color(255);*/
    colorMode(HSB);
    return color(map(gaps,1,12,0,255),220,220);
  }

}
