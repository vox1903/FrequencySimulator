float sizeR, x, y;
float topWaves = 10;
Wave[] waves = new Wave[10];
float outbounds;
float octave = 0;
var slider;

void setup() {
  //size(window.innerWidth, window.innerHeight);
  size(800, 500);
  background(0);
  x = width /2;
  y = height / 2 ;
  outbounds = PVector.sub(new PVector(x, y), new PVector(0, 0)).mag()*2 + 25;
  for(int i = 0; i < waves.length; i++){
      waves[i] = new Wave(width/2, height/2, 0);
      println('nuWav');
  }

  /*slider = createSlider(-3, 3, 0, 1);
  slider.position(10, 10);
  slider.style('width', '20%');*/
}

void draw() {
  background(0);
  //noStroke();
  //fill(100,30);
  //rect(0,0,width,height);
  fill(30);
  rect(10,10,72,12);
  fill(255,0,0);
  rect(11+(10 * octavePos()),11,10,10);
  fill(255);
  textSize(12);
  text("Octava: " + (octavePos()+1), 20, 35);

  for (int i = 0; i < waves.length; i++) {

    if (waves[i].gaps == 0) continue;
    waves[i].update();
    waves[i].draw();
    if (!waves[i].inbounds()){
      waves[i].osc[0].stop();
      waves[i].osc[1].stop();
      waves[i].osc[2].stop();
      waves[i].gaps = 0;
    }
  }
}

void newWave(int gaps) {
  for (int i = 0; i < waves.length; i++) {
    if (waves[i].gaps == 0) {
      waves[i].gaps = gaps;
      waves[i].play();
      return;
    };
  }
}

int getNumWaves() {
  int res = 0;
  for (int i = 0; i < waves.length; i++) {
    if (waves[i] != null) res++;
  }
  return res;
}

void keyPressed() {
  if (key == '1') newWave(1);
  if (key == 'q') newWave(2);
  if (key == '2') newWave(3);
  if (key == 'w') newWave(4);
  if (key == '3') newWave(5);
  if (key == '4') newWave(6);
  if (key == 'r') newWave(7);
  if (key == '5') newWave(8);
  if (key == 't') newWave(9);
  if (key == '6') newWave(10);
  if (key == 'y') newWave(11);
  if (key == '7') newWave(12);
  if (key == '8') newWave(13);
  if ((key == 'j') || (key == 'J')) octDown();
  if ((key == 'k') || (key == 'K')) octUp();
}

void octUp(){
  if(octave < 3) octave++;
}

void octDown(){
  if(octave > -3) octave--;
}

int octavePos(){
  if (octave == -3) return 0;
  if (octave == -2) return 1;
  if (octave == -1) return 2;
  if (octave == 0) return 3;
  if (octave == 1) return 4;
  if (octave == 2) return 5;
  if (octave == 3) return 6;
  return 4;
}
