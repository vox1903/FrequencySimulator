
class SimpleWave extends Wave{
  
  int gaps;
  SimpleWave(float x, float y, int gaps){
    pos = new PVector(x,y);
    radius = 5;
    this.gaps = gaps;
  }

  void update(){
    radius += 2;
  }


  void draw(){
    pushMatrix();
    pushStyle();
    translate(pos.x,pos.y);
    noFill();
    strokeWeight(5);
    stroke(getCol(),80);
    gapLipse(0,0,radius,gaps);
    popStyle();
    popMatrix();
  }

  void gapLipse(float x, float y, float radius, float gaps){
    float gapSize = 360/(2*gaps);
    float sizeArc = (360 - (gaps * gapSize)) / gaps;
    float start, end;
    for(int i = 0; i < gaps; i++){
      start = i * (sizeArc + gapSize);
      end = start + sizeArc;
      arc(x,y,radius,radius,radians(start),radians(end));
     }
  }

  color getCol(){
    if(gaps == 1) return color(255,0,0);
    if(gaps == 2) return color(0,255,0);
    if(gaps == 3) return color(0,0,255);
    if(gaps == 4) return color(255,255,0);
    if(gaps == 5) return color(0,255,255);
    if(gaps == 6) return color(255,0,255);
    return color(255);
  }

}