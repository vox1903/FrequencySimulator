var express = require('express');
var router = express.Router();


router.get('/', function(req, res, next) {
  res.render('index');
});
router.get('/ripple', function(req, res, next) {
  res.render('ripple');
});

module.exports = router;
