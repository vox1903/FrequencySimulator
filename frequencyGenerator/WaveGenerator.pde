import ddf.minim.*;
import ddf.minim.ugens.*;
import java.util.Iterator;


class WaveGenerator{

  ArrayList<Wave> waves;
  
  WaveGenerator(){
    waves = new ArrayList();
  }
  
  void createWave(WavesEnum typeWave,int posX,int posY,char pressedKey){
    switch (typeWave){
      case SIMPLEWAVE :
        int parts = 0;
        if (Character.isDigit(pressedKey)){ parts = Character.getNumericValue(pressedKey); }
        waves.add(new SimpleWave(posX, posY,parts));
        break;
       case SOUNDWAVE :
         waves.add(new SoundWave(posX, posY));
         break;
      default :
        return;
    }
  }
  
  
  
  void update(){
    Iterator<Wave> i = waves.iterator();
    while (i.hasNext()){
      Wave w = i.next();
      w.update();
      if (!w.inBounds()){
        i.remove();
      }
      w.draw();
    }
  }
  

}